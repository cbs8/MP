from django.db import models

# Create your models here.
from shop.models import Product


class Order(models.Model):
    first_name = models.CharField(max_length=50, verbose_name="Імя")
    last_name = models.CharField(max_length=50, verbose_name="Прізвище")
    email = models.EmailField()
    #address = models.CharField(max_length=250, blank = True, verbose_name="Адреса")
    #postal_code = models.CharField(max_length=20, blank = True)
    #city = models.CharField(max_length=100, blank = True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    paid = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Замовлення'
        verbose_name_plural = 'Замовлення'

    def __str__(self):
        return f'Замовлення {self.id}'

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='items')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='order_items')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="Вартість")
    quantity = models.PositiveIntegerField(default=1, verbose_name="Кількість")

    def __str__(self):
        return f'{self.id}'

    def get_cost(self):
        return self.price * self.quantity
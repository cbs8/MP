#from django.conf.urls import url
from django.urls import path, include
from . import views


app_name = 'shopping_orders'
urlpatterns = [
    path(r'create/', views.order_create, name='order_create'),
    #path(r'^create/$', views.order_create, name='order_create'),
]
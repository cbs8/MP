from django.shortcuts import render

# Create your views here.

from .models import OrderItem
from .forms import OrderCreateForm
from shopping_cart.cart import Cart


def order_create(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save()
            for item in cart:
                OrderItem.objects.create(order=order,
                                         product=item['product'],
                                         price=item['price'],
                                         quantity=item['quantity'])
            # очищення кошика
            cart.clear()
            return render(request, 'shopping_orders/order/created.html',
                          {'order': order})
            #return render(request, 'shopping_orders/order/created.html', {'order': order})
    else:
        form = OrderCreateForm
    return render(request, 'shopping_orders/order/create.html', {'cart': cart, 'form': form})
    #return render(request, 'shopping_orders/order/create.html, {'cart': cart, 'form': form})
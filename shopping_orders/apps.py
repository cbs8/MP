from django.apps import AppConfig


class ShoppingOrdersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'shopping_orders'

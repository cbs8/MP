
from django.contrib import admin
from django.urls import path, include

#from django.conf.urls import url
from . import views

app_name = 'shop'
urlpatterns = [
    path(r'', views.product_list, name='product_list'),
    path(r'about/', views.about, name='about'),
    #path(r'^$', views.product_list, name='product_list'),
    path(r'(?P<category_slug>[-\w]+)/',
        views.product_list,
        name='product_list_by_category'),
    path(r'(?P<id>\d+)/(?P<slug>[-\w]+)/',
        views.product_detail,
        name='product_detail'),
    path('Search/', views.Search.as_view(), name='Search'),
    #path('SearchProducts/', views.SearchProducts.as_view(), name='SearchProducts'),
    path('SearchNames/', views.SearchNamesView.as_view(), name='SearchNames'),

]

'''
urlpatterns = [
    url(r'^$', views.product_list, name='product_list'),
    url(r'^(?P<category_slug>[-\w]+)/$',
        views.product_list,
        name='product_list_by_category'),
    url(r'^(?P<id>\d+)/(?P<slug>[-\w]+)/$',
        views.product_detail,
        name='product_detail'),
]
'''

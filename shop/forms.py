from django import forms

from .models import CommentModel


class CommentForm(forms.ModelForm):
    """Form of comment goods."""
    class Meta:
        model = CommentModel
        fields = ('body', )
        #fields = ('name', 'email', 'body')
from django.shortcuts import render

# Create your views here.
from django.shortcuts import get_object_or_404
from .models import Category, Product, CommentModel
from shopping_cart.forms import CartAddProductForm

from .forms import CommentForm
#for search (from DZ_CBS33)
from django.views.generic import TemplateView, ListView
from django.db.models import Q

def product_list(request, category_slug=None):
    """ Загальний список (наявних) товарів."""
    category = None
    categories = Category.objects.all()
    products = Product.objects.filter(available=True)
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    return render(request,
                  'shop/product/list.html',
                  {'category': category,
                   'categories': categories,
                   'products': products})


'''def product_detail(request, id, slug):
    product = get_object_or_404(Product,
                                id=id,
                                slug=slug,
                                available=True)
    return render(request,
                  'shop/product/detail.html',
                  {'product': product})'''


def product_detail(request, id, slug):
    """ Опис (наявної) одиниці товару."""
    product = get_object_or_404(Product,
                                id=id,
                                slug=slug,
                                available=True)
    cart_product_form = CartAddProductForm()  #кнопка дадати в кошик

    comments = product.comments.filter(active=True)  #використовуємо related_name з моделі CommentModel
    if request.method == 'POST':
        # A comment was posted
        comment_form = CommentForm(data=request.POST)
        print(f'test comment1: {comment_form}')
        if comment_form.is_valid():
            print(f'test comment2: {comment_form}')
            # Create Comment object but don't save to database yet
            new_comment = comment_form.save(commit=False)
            # Assign the current post to the comment
            new_comment.product = product
            # Save the comment to the database
            new_comment.save()
            print(f'test comment3: {comment_form}')
    else:
        comment_form = CommentForm() # for metod GET

    return render(request,
                  'shop/product/detail.html',
                  {'product': product,
                   'cart_product_form': cart_product_form,
                   'comments': comments,
                   'comment_form': comment_form})


def about(request):
    """Publication of general information about project/company"""
    return render(request,'shop/about.html', {})

#(from DZ_CBS33)
class Search(TemplateView):
    """Product search by keyword (part of) in name, description DB (will be update for many fields)."""
    template_name = 'shop/search.html'
    def post(self , request):
        query = request.POST['search']
        result_list  = Product.objects.filter(Q(name__icontains=query) | Q(description__icontains=query))
        #result_list = Product.objects.filter(name=query)

        if result_list.count() != 0:
            context = {
                'result_list': result_list,
                'query': query,
            }
        else:
            context ={
                'empty': "Нічого не найдено :(",
                'query': query,
            }
        return render(request , 'shop/search_results.html' , context)


'''class SearchProducts(TemplateView):
    """Product search by name in DB."""
    template_name = 'shop/base.html'
    def post(self , request):
        query = request.POST['search']
        result_list  = Product.objects.filter(name=query)
        if result_list.count() != 0:
            context = {
                'result_list': result_list,
                'query': query,
            }
        else:
            context ={
                'empty': "Нічого не найдено :(",
                'query': query,
            }
        return render(request , 'shop/search_products.html' , context)'''


class SearchNamesView(ListView):
    """Product search by name in DB."""
    model = Product
    template_name = 'shop/search_names.html'

    def get_queryset(self):  # новый
        query = self.request.GET.get('q')
        object_list = Product.objects.filter(
            name=query
            #Q(name__icontains=query) | Q(description__icontains=query)
        )
        return object_list
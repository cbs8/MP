from django.db import models

# Create your models here.
from django.urls import reverse


class Category(models.Model):
    """Category of goods our shop."""
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Категорія'
        verbose_name_plural = 'Категорії'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:product_list_by_category',
                       args=[self.slug])


class Product(models.Model):
    """Detail description of goods."""
    category = models.ForeignKey(Category,
                                 on_delete=models.CASCADE,
                                 related_name='products')
    name = models.CharField(max_length=50,
                            db_index=True,
                            verbose_name="Найменування")
    slug = models.SlugField(max_length=200,
                            db_index=True)  #аліас продукта(його URL)?
    code = models.PositiveIntegerField(blank = True,
                                       verbose_name="артикул")
    stock = models.PositiveIntegerField(default=100,
                                        verbose_name="кількість товара") #default=100 for 'simply inputing'
    available = models.BooleanField(default=True,
                                    verbose_name="наявність товара")
    price = models.DecimalField(max_digits = 8,
                                decimal_places = 2,
                                verbose_name="ціна товара") #222222.22
    description = models.TextField(max_length=2000,
                                   blank = True,
                                   verbose_name="опис товару")
    image = models.ImageField(upload_to='media',
    #image = models.ImageField(upload_to='products/%Y/%m/%d',
                              blank = True,
                              verbose_name="фотографія товара")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    '''integer_field           = models.IntegerField()
    positive_small_field    = models.PositiveSmallIntegerField()
    big_integer_field       = models.BigIntegerField()
    float_field             = models.FloatField()
    binary_field            = models.BinaryField()
    file_field              = models.FileField(upload_to = 'file')'''
    #onsale = models.ManyToManyField(Sale)
    #user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        ordering = ('name',)
        # задаємо індекс для полів, тя будемо запитувати продукти за допомогою id та slug
        index_together = (('id', 'slug'),)
        verbose_name = 'Товар'
        verbose_name_plural = 'Товари'

    def __str__(self):
        return f"Назва : {self.name} Артикул : {self.code} Наявність : {self.available}"

    def get_absolute_url(self):
        return reverse('shop:product_detail', args=[self.id, self.slug])


class CommentModel(models.Model):
    """Users anonimus comment of goods."""
    product = models.ForeignKey(Product,
                             on_delete=models.CASCADE,
                             related_name='comments',
                             verbose_name="коментар товару")
    #name = models.CharField(max_length=80)
    #email = models.EmailField()
    body = models.TextField(verbose_name="відгук")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True) #for moderate comment

    class Meta:
        ordering = ('created',)
        verbose_name = 'Відгук'
        verbose_name_plural = 'Відгуки'

    def __str__(self):
        return f'Відну авторизованого користувача до {self.product}'


'''
class User(models.Model):
    name = models.CharField(max_length=10, blank = True, verbose_name = "Ім'я")
    surname = models.CharField(max_length=15, blank = True, verbose_name="Прізвище", null=True)
    email = models.EmailField(blank = True)
    date_time_buy = models.DateTimeField(auto_now_add=False, verbose_name="дата покупки", null=True)
    LastBuy = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f"Ім'я : {self.name} Прізвище : {self.surname}"

    class Meta:
        verbose_name = "Користувач"
        verbose_name_plural = "Користувачі"
'''

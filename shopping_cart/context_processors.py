from .cart import Cart

def cart(request):
    """Встановлення кошика як обєкта обробника контекстного процесора"""
    return {'cart': Cart(request)}
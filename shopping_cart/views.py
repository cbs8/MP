from django.shortcuts import render

# Create your views here.
from django.shortcuts import redirect, get_object_or_404
from django.views.decorators.http import require_POST
from shop.models import Product
from .cart import Cart
from .forms import CartAddProductForm


@require_POST #Дозволяємо лише POST запити
def cart_add(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = CartAddProductForm(request.POST)
    if form.is_valid():
        cd = form.cleaned_data
        cart.add(product=product,
                 quantity=cd['quantity'],
                 update_quantity=cd['update'])
    else:
        print(f'errrrrooooorrrrreeee: quantity <=0 ')
        #return HttpResponse('Invalid login')

    return redirect('shopping_cart:cart_detail')


def cart_remove(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product)
    return redirect('shopping_cart:cart_detail')


def cart_detail(request):
    cart = Cart(request)
    return render(request, 'shopping_cart/detail.html', {'cart': cart})

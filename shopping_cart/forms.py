from django import forms
from django.core.validators import ValidationError

#PRODUCT_QUANTITY_CHOICES = [(i, str(i)) for i in range(1, 11)]


def check_quantity(value):
    print(f'check quantity: {value}')
    if value <= 0:
        raise ValidationError("має бути позитивне число")


class CartAddProductForm(forms.Form):
    #quantity = forms.TypedChoiceField(choices=PRODUCT_QUANTITY_CHOICES, coerce=int, label = "Кількість" )
    quantity = forms.DecimalField(max_digits=5,
                                  decimal_places=0,
                                  label = "Кількість",
                                  help_text="Введіть необхідну кількість одиниць товару",
                                  validators=[check_quantity],
                                  )
    update = forms.BooleanField(required=False, initial=False, widget=forms.HiddenInput)